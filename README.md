# Introduction

An attempt to document and seek consensus for one or more Bitcoin Cash representational units that would be ISO-4217 compatible.

## Problem statement

...

- ...
- ...
- ...

## Proposed solution

Overview of the solution. List of things that would need to be done in order to adopt this proposal.

- Document the proposed units on public information websites.
- Submit the proposed information for inclusion into the ISO-4217 standard.
- Contact service providers to create awareness of the proposed units and their definition.
  - Wallets
  - Exchanges
  - Merchants

### Benefits

While it is difficult to assess the full benefits due to the social and cultural impacts, there are a large number of benefits that can be clearly described, such as:

- With multiple units available, users and services can choose the most suitable unit for their usecase.
- It is easier to use units with two or no decimals:
  - Similar to using `$5` instead of `0.000005 million USD` for a cup of coffee, you can now express `0.00002409 BCH` as `24 bits and 9 sats` or `2409 sats`.
  - Matching decimals with existing national currencies removes issues with number input at the point of sale.
  - Having similar form and structure to existing national currencies prevents misunderstandings and removes friction where culture and expectations wouldn't otherwise match.
- It is easier to integrate with existing software and services with valid ISO-4217 units:
  - Many software already supports ISO-4217 unit format, making it easier to add new units.
  - Having formalized ISO-4217 units automatically results in availability in some software.
  - Sales, accounting and similar software may be required by law or regulation to use ISO-4217 units.
- The units proposed are already well understood and present in the cryptocurrency community:
  - No need for existing users to learn new terminology.
  - Some wallets already support the proposed units as optional choices.
- Compared to previous proposals, this does not redefine the existing unit:
  - Usecases well served with large units can continue to use BCH.
  - Transition to the new units can happen over time as the demand for them goes up.
  - The risk of damaging the brand, value and network effects is significantly smaller.


## Proposed Units

### Bitcoin Cash

...

Name | Description
--- | --- 
Bitcoin Cash | Full name of the unit, in singular
Bitcoin Cash | Full name of the unit, in plural
Sat | Short form commonly used verbally, in singular
Sats | Short form commonly used verbally, in plural
? | Symbol to prepend?/append? after a number to indicate this unit
XCH? | Proposed unit code compatible with ISO-4217, to use in addition to the BCH ticker


### Satoshis

Bitcoin Cash (BCH) uses a different unit in the protocol for technical reasons.
This unit is called `Satoshis` in honor of the inventor of the protocol.
Currently, this is the smallest possible unit in the protocol.

Name | Description
--- | --- 
Satoshi | Full name of the unit, in singular
Satoshis | Full name of the unit, in plural
Sat | Short form commonly used verbally, in singular
Sats | Short form commonly used verbally, in plural
s? | Symbol to prepend?/append? after a number to indicate this unit
XSH? | Proposed unit code compatible with ISO-4217


### uBCH / Bits / Cash / Cashbits / Bitcash

...

Name | Description
--- | --- 
Satoshi | Full name of the unit, in singular
Satoshis | Full name of the unit, in plural
Bit? | Short form commonly used verbally, in singular
Bits? | Short form commonly used verbally, in plural
bȻȼϾͼ¢? | Symbol to prepend?/append? after a number to indicate this unit
X··? | Proposed unit code compatible with ISO-4217


## Impact of the proposal

### Initial upfront costs

Here we will document the initial and direct costs of adopting this proposal.

### Ongoing long-term costs

Here we will document what maintainance and long-term costs adoption of this proposal might have.

### Current users

Here we will document what users are already using some variation of this proposal to clearly show existing demand.

### Potential users

Here we will document what users are currently not well-served that might use our currency if we adopt this proposal.

### Missable opportunities

Here we will document what the impact would be for failing to adopt this proposal.


